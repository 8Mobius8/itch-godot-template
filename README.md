# Itch.io Jam Godot Template

## How to setup

1. Clone or Fork this template
`git clone https://gitlab.com/8mobius8/itch-jam-template`

2. Rename project
* Change `EXPORT_NAME` variable in `.gitlab-ci.yml` to desired name for exports.
This will change the name of the files pushed to itch.io
* While in Godot editor `Project > Project Settings > General tab > Application category > Config` 
change 'Name' property to desired name.

Uses barichello/godot-ci image to automate building game artifacts for you in gitlab piplines.
