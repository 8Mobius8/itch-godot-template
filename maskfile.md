# Tasks

## setup
> Setup up depedancies for ci, except for godot binary.

~~~sh
pip3 install gdtoolkit
~~~

## lint
> Lints the project and formats gdscript.

~~~sh
echo "Linting & Formating..."
gdformat .
gdlint .
~~~

## lint-definition-order
> Will use gdlint to print the order of definitions that should be written in a 
> gdscript file.

~~~sh
gdlint -v . 2>/dev/null | grep 'class-definitions-order'
```