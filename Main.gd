extends Spatial

export var rotation_speed = 15

func _process(delta):
    $Sprite3D.rotate_y(deg2rad(rotation_speed*delta))
